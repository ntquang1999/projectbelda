using FishNet.Component.Animating;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class NewCombatSystem : NetworkBehaviour
{
    private InputManager inputManager;
    private Character character;
    private Animator animator;
    private NetworkAnimator networkAnimator;

    [SerializeField]
    private WeaponState weaponState = WeaponState.Sheath;

    [SerializeField]
    private float amountOfSprintTimeBeforeSprintAttack;

    [SerializeField]
    private float sheathCooldownTime;

    [SerializeField]
    private int currentWeaponIndex;

    [SerializeField]
    private List<GameObject> backWeapons;

    [SerializeField]
    private List<GameObject> rightHandWeapons;

    [SerializeField]
    private List<GameObject> leftHandWeapons;

    private int attackIndex;
    private bool canCombo;

    private void Awake()
    {
        character = GetComponent<Character>();
        animator = GetComponentInChildren<Animator>();
        networkAnimator = GetComponentInChildren<NetworkAnimator>();
        inputManager = FindObjectOfType<InputManager>();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        currentWeaponIndex = 0;
        canCombo = false;
        attackIndex = 0;
        inputManager.ResetInput();
    }

    private void OnAttackPressed()
    {
        if (weaponState == WeaponState.Sheath && character.IsGrounded())
        {
            DrawWeapon();
        }
            
        if(((weaponState == WeaponState.Drawn) || (weaponState == WeaponState.Attacking && canCombo)) && character.IsGrounded())
        {
            Attack();
        }
    }

    private void DrawWeapon()
    {
        networkAnimator.SetTrigger("DrawWeapon");
        weaponState = WeaponState.Drawing;
        WeaponDrawnMovementLock(true);
        character.SetMovementMode(MovementMode.None);
    }

    private void SheathWeapon()
    {
        networkAnimator.SetTrigger("SheathWeapon");
        weaponState = WeaponState.Sheathing;
        character.SetMovementMode(MovementMode.None);
    }

    private void Attack()
    {
        if(canCombo) 
            attackIndex++; 
        else 
            attackIndex = 0;

        networkAnimator.SetTrigger("Attack");
        animator.SetInteger("AttackIndex", attackIndex);
        character.SetMovementMode(MovementMode.None);
        //animator.applyRootMotion = true;
        weaponState = WeaponState.Attacking;
        canCombo = false;
    }

    public void OnWeaponDrawn()
    {
        weaponState = WeaponState.Drawn;
        OnTransferWeaponToHand();
        character.SetMovementMode(MovementMode.Walking);
    }

    public void OnWeaponSheathed()
    {
        weaponState = WeaponState.Sheath;
        OnTransferWeaponToBack();
        character.SetMovementMode(MovementMode.Walking);
        WeaponDrawnMovementLock(false);
    }

    public void OnTransferWeaponToHand()
    {
        backWeapons[currentWeaponIndex].SetActive(false);
        rightHandWeapons[currentWeaponIndex].SetActive(true);
    }

    public void OnTransferWeaponToBack()
    {
        backWeapons[currentWeaponIndex].SetActive(true);
        rightHandWeapons[currentWeaponIndex].SetActive(false);
    }

    

    public void OnFinishAttack(int attackIndex)
    {
        if (attackIndex != this.attackIndex) return;

        canCombo = false;
        //animator.applyRootMotion = false;
        character.SetMovementMode(MovementMode.Walking);
        weaponState = WeaponState.Drawn;
    }

    public void OnFinishedSprintAttack()
    {
        
    }

    public void OnCanCombo()
    {
        canCombo = true;
    }

    private void Update()
    {
        if (!OnStartClientCalled) return;

        bool locomotion = (weaponState == WeaponState.Sheath || weaponState == WeaponState.Drawn);
        animator.SetBool("Locomotion", locomotion);
        animator.SetBool("WeaponDrawn", weaponState == WeaponState.Drawn);
        character.canEverSprint = weaponState == WeaponState.Sheath;
        character.canEverCrouch = weaponState == WeaponState.Sheath;

        HandleInput();
    }

    private void HandleInput()
    {
        if (inputManager.GetSprintInput()) OnSprintPressed();
        if (inputManager.GetAttackInput()) OnAttackPressed();
    }

    private void OnSprintPressed()
    {
        if (weaponState == WeaponState.Drawn)
            SheathWeapon();
    }

    private void WeaponDrawnMovementLock(bool lockState)
    {
        character.canEverCrouch = lockState;
    }
}
