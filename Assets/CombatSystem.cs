using FishNet.Component.Animating;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public enum WeaponState
{
    Sheath,
    Drawing,
    Drawn,
    Sheathing,
    Attacking,
    NoWeapon,
}

public class CombatSystem : NetworkBehaviour
{
    [SerializeField]
    private InputActionAsset inputActions;

    private InputAction attackInput;

    private InputAction jumpInput;

    private InputAction sprintInput;

    private Character character;
    private Animator animator;
    private NetworkAnimator networkAnimator;

    [SerializeField]
    private WeaponState weaponState = WeaponState.Sheath;

    [SerializeField]
    private float amountOfSprintTimeBeforeSprintAttack;

    [SerializeField]
    private float sheathCooldownTime;

    [SerializeField]
    private int currentWeaponIndex;

    [SerializeField]
    private List<GameObject> backWeapons;

    [SerializeField]
    private List<GameObject> rightHandWeapons;

    [SerializeField]
    private List<GameObject> leftHandWeapons;


    private bool isStillSprinting;
    private float sprintAttackTimer;
    private float sheathCoolDownTimer;
    private bool isHeavyWeapon;
    private int maxCombo;
    private int currentCombo;
    private bool canCombo;

    private void Awake()
    {
        character = GetComponent<Character>();
        animator = GetComponentInChildren<Animator>();
        networkAnimator = GetComponentInChildren<NetworkAnimator>();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        currentWeaponIndex = 0;

        attackInput = inputActions.FindAction("Attack");
        if(attackInput != null)
        {
            //attackInput.started += OnAttackPressed;
            attackInput.performed += OnAttackPressed;
            //attackInput.canceled += OnAttackPressed;

            attackInput.Enable();
        }

        sprintInput = inputActions.FindAction("Sprint");
        if (sprintInput != null)
        {
            //attackInput.started += OnAttackPressed;
            sprintInput.performed += OnSprintPressed;
            sprintInput.canceled += OnSprintReleased;

            sprintInput.Enable();
        }

        ResetSprintAttackTimer();

        animator.SetInteger("Weapon", 0);
        isHeavyWeapon = false;
        maxCombo = 2;

    }

    private void OnAttackPressed(InputAction.CallbackContext context)
    {
        if (!IsOwner) return;

        if(weaponState == WeaponState.Sheath && character.IsGrounded()  && !character.IsCrouching())
        {
            if (character.GetSpeed() <= character.maxWalkSpeed)
                DrawWeapon();
            else if(character.IsSprinting() && character.GetSpeed() > character.maxWalkSpeed)
            {
                if(sprintAttackTimer <= 0f) SprintAttack();
                else
                    DrawWeapon();
            }
        }

        if(weaponState == WeaponState.Drawn && character.IsGrounded())
        {
            currentCombo = 1;
            Attack(currentCombo);
        }

        if(weaponState == WeaponState.Attacking)
        {
            if(canCombo)
            {
                currentCombo++;
                Attack(currentCombo);
                canCombo = false;
            }
        }    
    }

    private void OnJumpPressed(InputAction.CallbackContext context)
    {

    }

    private void OnSprintPressed(InputAction.CallbackContext context)
    {
        if (!IsOwner) return;

        isStillSprinting = true;

        if(weaponState == WeaponState.Drawn && character.IsGrounded())
        {
            SheathWeapon();
        }
    }

    private void OnSprintReleased(InputAction.CallbackContext context)
    {
        if (!IsOwner) return;

        isStillSprinting = false;
        ResetSprintAttackTimer();
    }

    private void DrawWeapon()
    {
        Debug.Log("Draw");
        if (sheathCoolDownTimer > 0) return;

        if(isHeavyWeapon)
        {
            character.SetMovementMode(MovementMode.None);
        }

        
        character.canEverCrouch = false;
        character.canEverSprint = false;
        networkAnimator.SetTrigger("DrawWeapon");
        animator.SetBool("DrawingWeapon", true);
        weaponState = WeaponState.Drawing;
    }

    private void SprintAttack()
    {
        character.canEverJump = false;
        character.canEverCrouch = false;
        animator.SetBool("CanTransitionToIdle", false);
        networkAnimator.SetTrigger("SprintAttack");
        animator.SetBool("WeaponDrawn", true);
        weaponState = WeaponState.Attacking;
    }

    private void SheathWeapon()
    {
        Debug.Log("Sheath");
        networkAnimator.SetTrigger("SheathWeapon");
        animator.SetBool("SheathingWeapon", true);
        weaponState = WeaponState.Sheathing;
    }

    private void Attack(int attackIndex)
    {
        Debug.Log("Called Attack");
        //character.useRootMotion = true;
        character.SetMovementMode(MovementMode.None);
        //if(isHeavyWeapon)
        ToggleRootMotion(true);
        animator.SetBool("CanTransitionToIdle", false);
        networkAnimator.SetTrigger("Attack");
        animator.SetInteger("AttackIndex", attackIndex);
        weaponState = WeaponState.Attacking;
    }

    public void OnWeaponDrawn()
    {
        if (isHeavyWeapon)
        {
            character.SetMovementMode(MovementMode.Walking);
        }

        character.canEverSprint = true;
        if (weaponState != WeaponState.Drawing) return;
        weaponState = WeaponState.Drawn;
        animator.SetBool("WeaponDrawn", true);
        animator.SetBool("DrawingWeapon", false);
        animator.SetBool("CanTransitionToIdle", true);
        Debug.Log(character.GetSpeed());
        
        if (isStillSprinting) SheathWeapon();
    }

    public void OnWeaponSheathed()
    {
        OnTransferWeaponToBack();

        if (!IsOwner) return;

        character.canEverCrouch = true;
        if (weaponState != WeaponState.Sheathing) return;
        weaponState = WeaponState.Sheath;
        animator.SetBool("WeaponDrawn", false);
        animator.SetBool("SheathingWeapon", false);
        animator.SetBool("CanTransitionToIdle", true);
        ResetSprintAttackTimer();
        ResetSheathCooldownTimer();

        Debug.Log("sheathed");
    }

    public void OnTransferWeaponToHand()
    {
        backWeapons[currentWeaponIndex].SetActive(false);
        rightHandWeapons[currentWeaponIndex].SetActive(true);
        if(IsOwner)
            CallObserverTransferWeaponToHand();
    }

    public void OnTransferWeaponToBack()
    {
        backWeapons[currentWeaponIndex].SetActive(true);
        rightHandWeapons[currentWeaponIndex].SetActive(false);
        if (IsOwner)
            CallObserverTransferWeaponToBack();
    }

    [ServerRpc]
    public void CallObserverTransferWeaponToHand()
    {
        OnTransferWeaponToHand_Observer();
    }

    [ServerRpc]
    public void CallObserverTransferWeaponToBack()
    {
        OnTransferWeaponToBack_Observer();
    }

    [ObserversRpc(IncludeOwner = false, BufferLast = true)]
    public void OnTransferWeaponToHand_Observer()
    {
        OnTransferWeaponToHand();
    }

    [ObserversRpc(IncludeOwner = false, BufferLast = true)]
    public void OnTransferWeaponToBack_Observer()
    {
        OnTransferWeaponToBack();
    }

    public void OnFinishAttack()
    {
        Debug.Log("Called Finish");
        character.SetMovementMode(MovementMode.Walking);
        animator.SetBool("CanTransitionToIdle", true);
        weaponState = WeaponState.Drawn;
        canCombo = false;
        ToggleRootMotion(false);
        if (isStillSprinting)
        {
            SheathWeapon();
        }
    }

    public void OnFinishedSprintAttack()
    {
        character.canEverJump = true;
        canCombo = false;
        if (isStillSprinting)
        {
            animator.SetBool("CanTransitionToIdle", true);
            SheathWeapon();
        }
        else
        {
            animator.SetBool("CanTransitionToIdle", true);
            weaponState = WeaponState.Drawing;
            OnWeaponDrawn();
        }
    }

    public void OnCanCombo()
    {
        canCombo = true;
    }

    private void Update()
    {
        if (!IsOwner) return;

        if (isStillSprinting)
            sprintAttackTimer -= Time.deltaTime;

        sheathCoolDownTimer -= Time.deltaTime;

        bool isIdle = character.GetSpeed() == 0 && (weaponState == WeaponState.Drawn || weaponState == WeaponState.Sheath) && character.GetMovementMode() == MovementMode.Walking;
        animator.SetBool("IsIdle", isIdle);
    }

    private void ResetSprintAttackTimer()
    {
        sprintAttackTimer = amountOfSprintTimeBeforeSprintAttack;
    }

    private void ResetSheathCooldownTimer()
    {
        sheathCoolDownTimer = sheathCooldownTime;
    }

    private void ToggleRootMotion(bool state)
    {
        animator.applyRootMotion = state;
    }
}
