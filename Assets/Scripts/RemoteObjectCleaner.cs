using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteObjectCleaner : NetworkBehaviour
{
    public bool IsServerObject;

    public List<GameObject> GoToDestroy;
    public List<GameObject> GoToDisable;
    public List<Component> CpnToDestroy;
    public List<Behaviour> CpntoDisable;

    public override void OnStartClient()
    {
        base.OnStartClient();

        if (IsServerObject && IsServer) return;

        if (IsOwner) return;

        foreach (var element in GoToDestroy)
            Destroy(element);

        foreach (var element in CpnToDestroy)
            Destroy(element);

        foreach (var element in GoToDisable)
            element.SetActive(false);

        foreach (var element in CpntoDisable)
            element.enabled = false;
    }
}
