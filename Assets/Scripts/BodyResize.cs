using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyCharacterMovement;

public class BodyResize : MonoBehaviour
{
    public CharacterMovement characterMovement;

    public void ResizeCharacter(float radius, float height)
    {
        if (radius > 0) characterMovement.radius = radius;
        if (height > 0) characterMovement.height = height;
    }
}
