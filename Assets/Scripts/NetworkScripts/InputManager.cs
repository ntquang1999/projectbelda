using UnityEngine;
using UnityEngine.InputSystem;


public class InputManager : MonoBehaviour
{
	[Header("Character Input Values")]

	[SerializeField]
	private Vector2 move;
	[SerializeField]
	private Vector2 look;
	[SerializeField]
	private bool jump;
	[SerializeField]
	private bool sprint;
	[SerializeField]
	private bool crouch;
	[SerializeField]
	private bool attack;

	private void OnMove(InputValue value)
	{
		move = value.Get<Vector2>();
	}

	private void OnLook(InputValue value)
	{
		look = value.Get<Vector2>();
	}

	private void OnJump(InputValue value)
	{
		jump = value.isPressed;
	}

	private void OnSprint(InputValue value)
	{
		sprint = value.isPressed;
	}

	private void OnAttack(InputValue value)
	{
		attack = value.isPressed;
	}

	private void OnCrouch(InputValue value)
    {
		crouch = value.isPressed;
    }

	public Vector2 GetMoveInput()
    {
		return move;
    }

	public Vector2 GetLookInput()
    {
		return look;
    }

	public bool GetSprintInput()
    {
		return sprint;
    }

	public bool GetJumpInput()
    {
		bool jumpPressed = jump;
		jump = false;
		return jumpPressed;
    }

	public bool GetAttackInput()
	{
		bool attackPressed = attack;
		attack = false;
		return attackPressed;
	}

	public bool GetCrouchInput()
	{
		bool crouchPressed = crouch;
		crouch = false;
		return crouchPressed;
	}

	public void ResetInput()
    {
		move = Vector2.zero;
		look = Vector2.zero;
		jump = false;
		sprint = false;
		attack = false;
		crouch = false;
    }

}