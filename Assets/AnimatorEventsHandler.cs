using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorEventsHandler : NetworkBehaviour
{
    [SerializeField]
    private CombatSystem combatSystem;


    public void WeaponDrawn()
    {
        if (!IsOwner) return;
        combatSystem.OnWeaponDrawn();
    }

    public void WeaponSheathed()
    {
        combatSystem.OnWeaponSheathed();
    }

    public void TransferWeaponToHand()
    {
        combatSystem.OnTransferWeaponToHand();
    }

    public void FinishAttack()
    {
        if (!IsOwner) return;
        combatSystem.OnFinishAttack();
    }

    public void FinishSprintAttack()
    {
        if (!IsOwner) return;
        combatSystem.OnFinishedSprintAttack();
    }

    public void CanCombo()
    {
        combatSystem.OnCanCombo();
    }
}
