using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewAnimationEventHandler : NetworkBehaviour
{
    [SerializeField]
    private NewCombatSystem newCombatSystem;


    public void WeaponDrawn()
    {
        if (!IsOwner) return;
        newCombatSystem.OnWeaponDrawn();
    }

    public void WeaponSheathed()
    {
        newCombatSystem.OnWeaponSheathed();
    }

    public void TransferWeaponToHand()
    {
        newCombatSystem.OnTransferWeaponToHand();
    }

    public void FinishAttack(int attackIndex)
    {
        if (!IsOwner) return;
        newCombatSystem.OnFinishAttack(attackIndex);
    }

    public void FinishSprintAttack()
    {
        if (!IsOwner) return;
        newCombatSystem.OnFinishedSprintAttack();
    }

    public void CanCombo()
    {
        newCombatSystem.OnCanCombo();
    }
}
